﻿using System;
using System.Collections.Generic;

namespace PrintValue
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<int, string> NumOneToNineteenAndThousand = new Dictionary<int, string> { {0, "Zero"}, { 1,"One"}, 
                { 2, "Two"}, { 3, "Three" }, { 4, "Four"}, { 5, "Five"}, {6, "Six"}, { 7, "Seven"}, { 8, "Eight"}, { 9, "Nine"}, 
                { 10, "Ten"}, { 11, "Eleven"}, { 12, "Twelve"}, { 13, "Thirteen"}, { 14, "Fourteen"}, { 15, "Fifteen"}, 
                { 16, "Sixteen"}, { 17, "Seventeen"}, { 18, "Eighteen"}, { 19, "Nineteen"}, { 1000, "One thousand"} };

            Dictionary<int, string> ForTwoDigitNumber = new Dictionary<int, string> { { 2, "Twenty"}, { 3, "Thirty"}, { 4, "Fourty"},
                { 5, "Fifty" }, { 6, "Sixty"}, {7, "Seventy" }, { 8, "Eighty"}, { 9, "Ninety"} };

            Dictionary<int, string> ForThreeDigit = new Dictionary<int, string> { { 1, "One hundred"}, { 2, "Two hundred" },
                {3, "Three hundred" }, {4, "Four hundred"}, {5, "Five hundred"}, {6, "Six hundred"}, {7, "Seven hundred"}, {8,
                "Eight hundred"}, {9, "Nine hundred"}};

            string InputNumber = Console.ReadLine();

            int num = int.Parse(InputNumber);

            if ((num >= 0 && num <= 19) || num == 1000)
                Console.WriteLine(NumOneToNineteenAndThousand[num]);

            else if (InputNumber.Length == 2)
            {
                if(num%10 !=0)
                   Console.WriteLine("{0} {1}", ForTwoDigitNumber[InputNumber[0] - 48], 
                       NumOneToNineteenAndThousand[InputNumber[1] - 48]); 
                else
                    Console.WriteLine("{0}", ForTwoDigitNumber[InputNumber[0] - 48]);
            }
            else
            {
                int NewNumber = num % 100;
                Console.Write("{0} ", ForThreeDigit[InputNumber[0] - 48]);
                if (NewNumber == 0)
                    Console.WriteLine();
                else if (NewNumber > 0 && NewNumber < 10)
                    Console.WriteLine(NumOneToNineteenAndThousand[InputNumber[2] - 48]);
                else if (NewNumber >= 10 && NewNumber <= 19)
                    Console.WriteLine(NumOneToNineteenAndThousand[NewNumber]);
                else
                {
                    if (NewNumber % 10 != 0)
                        Console.WriteLine("{0} {1}", ForTwoDigitNumber[InputNumber[1] - 48],
                            NumOneToNineteenAndThousand[InputNumber[2] - 48]);
                    else
                        Console.WriteLine("{0}", ForTwoDigitNumber[InputNumber[1] - 48]);
                }
            }
        }
    }
}
